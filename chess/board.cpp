//
//  board.cpp
//  chess
//
//  Created by Henry Lin on 6/20/14.
//  Copyright (c) 2014 hlin117. All rights reserved.
//

#include "board.h"
#include "piece.h"
#include "pawn.h"

Board::Board() {
	width = WIDTH;
	for(int rank = 0; rank < WIDTH; rank++) {
		for(int file = 0; file < WIDTH; file++) {
			squares[rank][file] = new Square(rank, file);
		}
	}
}

Board::~Board() {
	for(int rank = 0; rank < WIDTH; rank++) {
		for(int file = 0; file < WIDTH; file++) {
			delete squares[rank][file];
		}
	}
}

bool Board::addPiece(int x, int y, Piece::Color color, Piece::Type type) {
	
	if(squares[x][y]->piece) return false;
	
	Piece * piece = NULL;
	switch(type) {
		case(Piece::pawn):
			piece = new Pawn(x, y, color);
			break;
		default:
			cout << "Not implemented yet" << endl;
	}
	squares[x][y]->piece = piece;
	
	return true;
}

bool Board::movePiece(int startX, int startY, int endX, int endY) {
	Piece * piece = squares[startX][startY]->piece;
	if(!piece) return false;
	
	// Need to check whether the move is actually legal
	const vector<Piece * > * pieceList = getPieceList();
	if(piece->isLegalMove(endX, endY, -1, -1, pieceList)) {
    	Piece * piece = squares[startX][startY]->piece;
    	squares[startX][startY]->piece = NULL;
    	squares[endX][endY]->piece = piece;
    	piece->set(endX, endY);
		
		Pawn * pawn = dynamic_cast<Pawn *>(piece);
		if(pawn) pawn->hasMoved = true;
		
		delete pieceList;
		return true;
	}
	delete pieceList;
	return false;
}


const vector<Piece *> * Board::getPieceList() {
	vector<Piece *> * output = new vector<Piece *>();
	for(int file = 0; file < WIDTH; file++) {
		for(int rank = 0; rank < WIDTH; rank++) {
			if(squares[rank][file]->piece)
				output->push_back(squares[rank][file]->piece);
		}
	}
	return output;
}


string Board::toString() const {
	string output = "";
	for(int file = 0; file < WIDTH; file++) {
		output += SEP;
		for(int rank = 0; rank < WIDTH; rank++) {
			Piece * piece = squares[rank][file]->piece;
			char c = (piece == NULL) ? NON : piece->icon;
			output.push_back(c);
			output += SEP;
		}
		output += "\n";
	}
	return output;
}