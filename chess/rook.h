//
//  rook.h
//  chess
//
//  Created by Henry Lin on 7/7/14.
//  Copyright (c) 2014 hlin117. All rights reserved.
//

#ifndef __chess__rook__
#define __chess__rook__

#include <iostream>
#include "piece.h"

/**
 * TODO: Create the functionality for En passant.
 */
class Rook : public Piece {

	
public:

	/**
	 * Inherited public properties:
	 * int x, y
	 * char icon
	 * Type type
	 * Color color
	 */
	Rook() : Piece() { /* nothing */ }
	Rook(int x, int y, Color color);
	virtual ~Rook() { /* nothing */ }
	bool hasMoved;
	
	/**
	 * Returns true if the move is legal.
	 */
	virtual bool isLegalMove(int endX, int endY, int kingX, int kingY,
							 const vector<Piece *> * pieceList) const;
	
	
private:
	
};



#endif /* defined(__chess__rook__) */
