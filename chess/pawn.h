//
//  pawn.h
//  chess
//
//  Created by Henry Lin on 6/19/14.
//  Copyright (c) 2014 hlin117. All rights reserved.
//

#ifndef chess_pawn_h
#define chess_pawn_h

#include "piece.h"
#include <utility>
#include <vector>

using std::vector;
using std::pair;

/**
 * TODO: Create the functionality for En passant.
 */
class Pawn : public Piece {

	
public:

	/**
	 * Inherited public properties:
	 * int x, y
	 * char icon
	 * Type type
	 * Color color
	 */
	Pawn() : Piece() { /* nothing */ }
	Pawn(int x, int y, Color color);
	virtual ~Pawn() { /* nothing */ }
	bool hasMoved;
	
	/**
	 * Returns true if the move is legal.
	 */
	virtual bool isLegalMove(int endX, int endY, int kingX, int kingY,
							 const vector<Piece *> * pieceList) const;
	
	
private:
	
	/**
	 * Determines whether the general direction of the pawn
	 * is correct, based upon the pawn's color.
	 * White pawns may increase in file (y increases), while
	 * black pawns may decrease in file.
	 */
	bool correctOrient(int endY) const;
};

#endif
