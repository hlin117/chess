//
//  main.cpp
//  chess
//
//  Created by Henry Lin on 6/16/14.
//  Copyright (c) 2014 hlin117. All rights reserved.
//

#include <iostream>
#include <time.h>
#include "writer.h"
#include <unistd.h>
#include "piece.h"
#include "board.h"
#include <vector>

using namespace std;

int main(int argc, const char * argv[]) {
	
	/*
	Board * board = new Board();
	string boardString = board->toString();
	cout << boardString << endl;
	
	board->addPiece(4, 4, Piece::white, Piece::pawn);
	board->addPiece(5, 4, Piece::black, Piece::pawn);
	string pawnString = board->toString();
	cout << "New board: \n" + pawnString << endl;
	 
	 // TODO: Test whether moving a piece has any effect
	delete board;
	 */
	
	vector<Piece *> * pieceList = new vector<Piece *>;
	Pawn * pawn = new Pawn(0, 0, Piece::white);
	Pawn * opponent = new Pawn(1, 1, Piece::black);
	pieceList->push_back(pawn);
	pieceList->push_back(opponent);
	
	// Tests general movement
	if(!pawn->isLegalMove(0, 1, -1, -1, pieceList)) {
		cout << "Warning: Test failed." << endl;
	}
	
	// Tests general movement
	if(!pawn->isLegalMove(0, 2, -1, -1, pieceList)) {
		cout << "Warning: Test failed." << endl;
	}
	
	// Black piece increasing in file
	if(opponent->isLegalMove(1, 2, -1, -1, pieceList)) {
		cout << "Warning: Test failed." << endl;
	}
	
	// Black piece decreasing in file
	if(!opponent->isLegalMove(1, 0, -1, -1, pieceList)) {
		cout << "Warning: Test failed." << endl;
	}
	
	// Tests taking a piece
	if(!pawn->isLegalMove(1, 1, -1, -1, pieceList)) {
		cout << "Warning: Test failed." << endl;
	}
	
	
	// Placing piece in front of white pawn
	Pawn * newGuy = new Pawn(0, 1, Piece::black);
	pieceList->push_back(newGuy);
	if(pawn->isLegalMove(0, 1, -1, -1, pieceList)) {
		cout << "Warning: Test failed." << endl;
	}
	
	
	
	delete pawn;
	delete pieceList;
}

