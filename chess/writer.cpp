//
//  writer.cpp
//  chess
//
//  Created by Henry Lin on 6/16/14.
//  Copyright (c) 2014 hlin117. All rights reserved.
//

#include "writer.h"
#define CLEAR '\b'

Writer::Writer() {
	numChars = 0;
}

Writer::~Writer() {
	clear();
}

void Writer::clear() {
	/* nothing */
}


Writer & Writer::operator=(const Writer & other) {
	if(this != &other) {
		clear();
		copy(other);
	}
	return *this;
}

void Writer::copy(const Writer & other) {
	numChars = other.numChars;
}

/**
 * Clears numChars characters from the console.
 */
// TODO: Make sure this number doesn't go below zero
void Writer::purge(int _numChars) {
	cout << string(_numChars, CLEAR) << flush;
	numChars -= _numChars;
}

/**
 * Clears the whole entire console.
 */
void Writer::purge() {
	cout << string(numChars, CLEAR) << flush;
	numChars = 0;
	
}

/**
 * Writes the string output to the console.
 */
void Writer::write(string output) {
	cout << output;
	numChars = (int)output.size();
}

/**
 * Calls purge(int) and write(string).
 */
void Writer::purgeWrite(int numChars, string output) {
	purge(numChars);
	write(output);
}

/**
 * Calls purge() and write(string).
 */
void Writer::purgeWrite(string output) {
	purge();
	write(output);
}
