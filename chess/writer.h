//
//  writer.h
//  chess
//
//  Created by Henry Lin on 6/16/14.
//  Copyright (c) 2014 hlin117. All rights reserved.
//

#ifndef __chess__writer__
#define __chess__writer__

#include <iostream>
#include <string>

using std::string;
using std::cout;
using std::flush;

class Writer {
public:
	Writer();
	~Writer();
	void clear();
	Writer & operator=(const Writer & other);
	void copy(const Writer & other);
	void purge(int _numChars);
	void purge();
	void write(string output);
	void purgeWrite(int numChars, string output);
	void purgeWrite(string output);
	
private:
	int numChars;
};

#endif /* defined(__chess__writer__) */
