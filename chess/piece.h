//
//  piece.h
//  chess
//
//  Created by Henry Lin on 6/19/14.
//  Copyright (c) 2014 hlin117. All rights reserved.
//

#ifndef chess_piece_h
#define chess_piece_h

#define WIDTH 8

#include <utility>
#include <vector>
#include <iostream>

class Square;

using std::vector;
using std::pair;
using std::cout;
using std::endl;

class Piece {
	
// General enums that belong to pieces, in general
public:
	enum Color {black, white};
	enum Type {pawn, rook, knight, bishop, king, queen};
	
	// Properties
	int x, y;
	char icon;
	Color color;
	Type type;

	// Functions
	virtual ~Piece() { /* nothing */ }
	Piece() { x = y = -1; color = black; }
	Piece(int x, int y, Color color) {
		this->x = x;
		this->y = y;
		this->color = color;
	}

	/**
	 * Shortcut method to change the x and y values of the piece.
	 */
	void set(int x, int y) { this->x = x; this->y = y; }
	
	/**
	 * Inherited function. Given an array of pieces, and
	 * the location of the king, determines whether the given piece 
	 * could move to the input square.
	 */
	virtual bool isLegalMove(int x, int y, int kingX, int kingY,
							 const vector<Piece *> * pieceList) const = 0;
	
	
protected:
	/**
	 * Checks whether the destination square is within the bounds
	 * of the chess board.
	 */
	bool withinBounds(int endX, int endY) const {
		return (endX >= 0 && endY >= 0 && endX < WIDTH && endY < WIDTH);
	}

	/**
	 * Given a list of pieces, checks whether there is a piece
	 * on the given input coordinates.
	 */
	bool pieceOnCoord(const vector<Piece *> * pieceList, int endX, int endY) const {
		for(auto piece : *pieceList) {
			if(piece->x == endX && piece->y == endY) return true;
		}
		return false;
	}

	/**
	 * Given a list of pieces, checks whether there is a piece
	 * on the given input coordinates. ALSO checks whether it is of the input color. If it is, return true.
	 */
	bool pieceOnCoord(const vector<Piece *> * pieceList, int endX, int endY, Color color) const {
		for(auto piece : *pieceList) {
			if(piece->x == endX && piece->y == endY && piece->color == color) return true;
		}
		return false;
	}
	
	/**
	 * Inherited function. Given the destination coordinates,
	 * the location of the king, and the board, determines
	 * whether moving to the destination would endanger
	 * the king.
	 * 
	 * TODO. This function is not implemented yet.
	 */
    bool endangersKing(int endX, int endY, int kingX, int kingY,
    						 const vector<Piece *> * pieceList) const {
		cout << "Warning: endangersKing is not implemented." << endl;
    	return false;
    }
};

#endif
