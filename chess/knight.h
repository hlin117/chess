//
//  knight.h
//  chess
//
//  Created by Henry Lin on 7/6/14.
//  Copyright (c) 2014 hlin117. All rights reserved.
//

#ifndef __chess__knight__
#define __chess__knight__

#include <iostream>
#include "piece.h"
#include <vector>

class Knight : public Piece {

	
public:
	
	/**
	 * Inherited public properties:
	 * int x, y
	 * char icon
	 * Type type
	 * Color color
	 */
	Knight() : Piece() { /* nothing */ }
	Knight(int x, int y, Color color);
	virtual ~Knight() { /* nothing */ }
	
	/**
	 * Returns true if the move is legal.
	 */
	virtual bool isLegalMove(int endX, int endY, int kingX, int kingY,
							 const vector<Piece *> * pieceList) const;
	
};



#endif /* defined(__chess__knight__) */
