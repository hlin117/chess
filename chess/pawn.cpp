//
//  pawn.cpp
//  chess
//
//  Created by Henry Lin on 6/19/14.
//  Copyright (c) 2014 hlin117. All rights reserved.
//

#include "piece.h"
#include "pawn.h"
#include <cmath>

#define PAWNREP(color) color == black ? 'p' : 'P'

Pawn::Pawn(int x, int y, Color color) : Piece(x, y, color) {
	hasMoved = false;
	icon = PAWNREP(color);
	type = pawn;
}

bool Pawn::isLegalMove(int endX, int endY, int kingX, int kingY, 
						const vector<Piece *> * pieceList) const {
	if(endangersKing(endX, endY, kingX, kingY, pieceList)) return false;
	if(!withinBounds(endX, endY)) return false;
	if(!correctOrient(endY)) return false;
	
	
	// Need to check whether the pawn could advance 2 squares
	if(abs(endY - y) == 2) {
		if(hasMoved || endX != x) return false;
    	int eligibleY = color == white ? y + 2 : y - 2;
		if(eligibleY != endY) return false;
		return !pieceOnCoord(pieceList, endX, endY);
	}
	
	// Need to check whether the pawn is simply advancing one square
	else if(abs(endY - y) == 1 && endX == x) {
		int eligibleY = color == white ? y + 1 : y - 1;
		if(eligibleY != endY) return false;
		return !pieceOnCoord(pieceList, endX, endY,
							 color == white ? black : white);
	}
	
	// Need to check whether the pawn can take a piece (go diagonally)
	else if(endX != x) {
		if(abs(endY - y) != 1 && abs(endX - x) != 1) return false;
		return pieceOnCoord(pieceList, endX, endY);
	}
	
	return false;
}


bool Pawn::correctOrient(int endY) const {
	return color == white ? y < endY : y > endY;
}

