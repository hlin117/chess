//
//  rook.cpp
//  chess
//
//  Created by Henry Lin on 7/7/14.
//  Copyright (c) 2014 hlin117. All rights reserved.
//

#include "rook.h"

#define ROOKREP(color) color == black ? 'r' : 'P'

Rook::Rook(int x, int y, Color color) : Piece(x, y, color) {
	hasMoved = false;
	icon = ROOKREP(color);
	type = pawn;
}

bool Rook::isLegalMove(int endX, int endY, int kingX, int kingY,
					   const vector<Piece *> * pieceList) const {
	if(endangersKing(endX, endY, kingX, kingY, pieceList)) return false;
	if(!withinBounds(endX, endY)) return false;
	
	/**
	 * Find whether the rook is moving vertically or horizontally,
	 * or even neither.
	 */
	bool vertMove = abs(x - endX) == 0 && abs(y - endY != 0);
	bool horzMove = abs(x - endX) != 0 && abs(y - endY == 0);
	if(!(vertMove || horzMove)) return false;
	
	/**
	 * Need to make sure that there isn't a piece that isn't
	 * in the same line of fire as the rook.
	 */
	if(vertMove) {
		
	}


}