//
//  board.h
//  chess
//
//  Created by Henry Lin on 6/19/14.
//  Copyright (c) 2014 hlin117. All rights reserved.
//

#ifndef chess_board_h
#define chess_board_h

#define SEP "|" // String rep character to separate cells
#define NON '.' // Character to represent empty square
#define WIDTH 8

#include <string>
#include "piece.h"
#include "pawn.h"
#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::string;
using std::vector;


/**
 * Describes the data structure that holds the pieces.
 * Supports moving pieces, as well as adding pieces to
 * the board.
 *
 * Also supports converting the board into a readable
 * string format.
 */
class Board {
	
private:
	
	// Misc. enums and definitions
	
	/**
	 * odd squares are white, even squares are black.
	 * On a standard 8 x 8 chessboard, a1 is black, so in
	 * this coordinate system, (0, 0) is black.
	 *
	 * Furthermore, the white rook is positioned on
	 * the square a1.
	 *
	 * Any x coordinate represents a rank, and
	 * any y coordinate represents a file.
	 */
	struct Square {
		Piece * piece;
		int coordX;
		int coordY;
		
		Square() {
			piece = NULL;
			coordX = coordY = -1;
		}
		
		Square(int x, int y) {
			piece = NULL;
			coordX = x;
			coordY = y;
		}
		
	};
	
	
	// Private members
	int width;
	Square * squares[WIDTH][WIDTH];
	Square * blackKingSquare;
	Square * whiteKingSquare;
	
	// Private functions
	const vector<Piece *> * getPieceList();
	
/**
 * TODO: Not sure if creating an operator=, copy, and
 * clear function would be appropriate.
 */
public:
	Board();
	~Board();
	
	/**
	 * Adds a piece to the chessboard. Returns true if and only
	 * if there is no piece on the destination square.
	 */
	bool addPiece(int x, int y, Piece::Color, Piece::Type type);
	
    /**
     * Returns true if the piece was able to move from
     * the start square to the ending square.
     */
	bool movePiece(int startX, int startY, int endX, int endY);
	
	/**
	 * Returns a string representation of the board.
	 */
	string toString() const;
	
};

#endif
