//
//  knight.cpp
//  chess
//
//  Created by Henry Lin on 7/6/14.
//  Copyright (c) 2014 hlin117. All rights reserved.
//

#include "knight.h"
#include <cmath>
#define KNIGHTREP(color) color == black ? 'n' : 'N'

Knight::Knight(int x, int y, Color color) {
	icon = KNIGHTREP(color);
	type = knight;
}


/**
 * Returns true if the move is legal. In the case of a knight,
 * we only need to make sure that the square of interest
 * is not occupied by a piece of the same color.
 *
 */
bool Knight::isLegalMove(int endX, int endY, int kingX, int kingY,
						 const vector<Piece *> * pieceList) const {
	if(endangersKing(endX, endY, kingX, kingY, pieceList)) return false;
	if(!withinBounds(endX, endY)) return false;
	
	if(!((abs(x - endX) == 2) && abs(y - endY) == 1) ||
	   ((abs(x - endX) == 1) && abs(y - endY) == 2)) return false;
	
	return !pieceOnCoord(pieceList, endX, endY, color);
}